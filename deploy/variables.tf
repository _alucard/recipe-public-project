variable "prefix" {
  default = "raad"
}

variable "project" {
  default = "recipe-public-project"
}

variable "contact" {
  default = "yakir.ort@gmail.com"
}

